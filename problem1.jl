using Markdown
using InteractiveUtils

using Flux, Images # External Packages

using DelimitedFiles, Random, Statistics, Printf, LinearAlgebra

md"""
# Intro
"""

md"""
# Problem 1
"""

# Functions for Problem 1

	"""
    train_test_split returns indecies for for p fraction to train and (1-p) fraction to test

    returns (train, test)
    """

function train_test_split(data, p=0.8)
    rows = size(data)[1]
    train = randsubseq(1:rows,p)
    test = setdiff(1:rows, train)
    return train, test
end

"""
    load_real_estate loads data for the first problem
    
    returns (data_array, headers)
"""

function load_real_estate(; T=Float64)
    readdlm("Real estate.csv", ',', T; header = true)
end

# Functions for Problem 1
"""
    rescale_features! rescales features to rune from 0 to 1
    
    returns (data_array, headers)
"""
function linear_rescale_features!(X)
    for col in eachcol(X)
        min, max = extrema(X)
        @. col = 2*(col - min)/(max-min) - 1 
    end
    return X
end

"""
    make_linear_regression_mlp returns mlp depding on the number of features used.
    
    layers is a Vector of the number of cells per layer
"""
function make_linear_regression_mlp(nfeatures::Integer, layers::Vector{<:Integer})
    return Chain(
        Dense(nfeatures, layers[1]),
        (Dense(layers[l-1],layers[l]) for l in 2:length(layers))...,
        Dense(layers[end],1)
    )
end

"""
    make_linear_loss_function returns the loss function used for lines mlp
"""
function make_linear_loss(model; λ=1)
    function linear_losss(x,y)
        predict = make_linear_model_prediction(model)
        
        l2 = λ .* predict(one.(x)) .^ 2 |> mean# L2 regularization
        
        yp = predict(x)
        return Flux.Losses.mse(yp, y) + l2
    end
    return linear_losss
end

"""
    make_linear_opt returns the optimizer function used for lines mlp
"""
function make_linear_opt(lin_model, η = 0.1)
    Flux.Descent(η)
end

"""
    train_linear_model makes a model for some epochs
"""
function train_linear_model!(loss, ps, Xtrain, ytrain, opt, epochs)
    for epoch in 1:epochs
        Flux.Optimise.train!(loss, ps, [(Xtrain, ytrain),], opt)
#         @info loss(Xtrain, ytrain)
    end
end

"""
    make_model_prediction makes a linear model's prediction function
"""
function make_linear_model_prediction(model)
    function linear_predict(x)
        reduce(vcat, map(model,eachrow(x)))
    end
    return linear_predict
end

"""
    test_model_performance tests the model's performatce on a test set
"""
function test_model_performance(model, X, y)
    predict = make_linear_model_prediction(model)
    ŷ = predict(X)
    total_loss = make_linear_loss(model)(X,y)
    
    println("Final Loss: $(total_loss)")
    println("Predicted ","\t","Real Value\t","% Diff    ")
    
    for (ypr,yr) in sort(zip(ŷ,y) |> collect, by=x->x[2])
        difference = (ypr-yr)/yr*100
        @printf "%10.2f\t%10.2f\t%10.2f\n" ypr yr difference
    end
end

	md"## Problem 1 Functions"
end


md"""
## Problem 1 Solution
"""

let data, headers, p = 0.8, 
    features = [2,3,4,5], # used for predicting!,
    goal = [8], # feature to predict 
    layers = [6,6], # size of hidden layers
    η = 0.001, # learning rate
    λ = 0.02, # L2 reg
    epochs = 500
    
    data, headers = load_real_estate(; T=Float32) # get data!
        
    model = make_linear_regression_mlp(length(features), layers)
    
    train, test = train_test_split(data, p) # split data into train and test via indicies
    
    X = data[:,features]
    linear_rescale_features!(X) # rescale features
    
    y = data[:,goal]
    
    ps = params(model)
    loss = make_linear_loss(model;λ=λ)
    
    opt = make_linear_opt(model, η)
    
    train_linear_model!(loss, ps, X[train,:], y[train,:], opt, epochs)
    
    test_model_performance(model, X[test,:], y[test,:])
        
end


